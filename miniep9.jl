using LinearAlgebra
using Test

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    dim = size(M)
    if dim[1] != dim[2]
        println("Insira uma matriz quadrada")
	return -1
    else
        N = dim[1]
    end
    c = Matrix{Int}(I,N,N)
    while p > 0
        c = multiplica(c, M)
        p = p - 1
    end
    return c
end

function matrix_pot_by_squaring(M, p)
    dim = size(M)
    if dim[1] != dim[2]
	println("Insira uma matriz quadrada")
	return -1
    else
    	N = dim[1]
    end
    if p == 0
	return Matrix{Int}(I,N,N)
    elseif p == 1
	return M
    elseif p > 1 && p % 2 == 0
	return matrix_pot_by_squaring(multiplica(M,M),p/2)
    elseif p > 1 && p % 2 == 1
        return multiplica(M,matrix_pot_by_squaring(multiplica(M,M),(p-1)/2))
    end
end

function testa_pot()
    @test matrix_pot([1 2;3 4], 0) == [1 0; 0 1]
    @test matrix_pot([1 2;3 4], 1) == [1 2; 3 4]
    @test matrix_pot([1 2;3 4], 2) == [7 10; 15 22]
    @test matrix_pot_by_squaring([1 2;3 4], 0) == [1 0; 0 1]
    @test matrix_pot_by_squaring([1 2;3 4], 1) == [1 2; 3 4]
    @test matrix_pot_by_squaring([1 2;3 4], 2) == [7 10; 15 22]
    println("Fim dos testes")
end


function compare_times()
    M = Matrix{Int}(LinearAlgebra.I, 100, 100)
    @time matrix_pot(M, 20)
    @time matrix_pot_by_squaring(M, 20)
end

